from django.contrib.auth.models import User
from django.db import models


# Create your models here.

class Category(models.Model):
    '''分组'''
    STATUS_NORMAL = 1
    STATUS_DELETE = 0
    STATUS_ITEMS = (
        (STATUS_NORMAL,'正常'),
        (STATUS_DELETE,'删除'),
    )

    name = models.CharField(max_length=50,verbose_name='名称')
    #正短整数或0类型，类似于PositiveIntegerField，取值范围依赖于数据库特性，[0 ,32767]的取值范围对Django所支持的数据库都是安全的。
    status = models.PositiveSmallIntegerField(default=STATUS_NORMAL,choices=STATUS_ITEMS,
        verbose_name='状态')
    is_nav = models.BooleanField(default=False,verbose_name='是否为导航')
    '''
    on_delete=None,               # 删除关联表中的数据时,当前表与其关联的field的行为
    on_delete=models.CASCADE,     # 删除关联数据,与之关联也删除
    on_delete=models.DO_NOTHING,  # 删除关联数据,什么也不做
    on_delete=models.PROTECT,     # 删除关联数据,引发错误ProtectedError
    # models.ForeignKey('关联表', on_delete=models.SET_NULL, blank=True, null=True)
    on_delete=models.SET_NULL,    # 删除关联数据,与之关联的值设置为null（前提FK字段需要设置为可空,一对一同理）
    # models.ForeignKey('关联表', on_delete=models.SET_DEFAULT, default='默认值')
    on_delete=models.SET_DEFAULT, # 删除关联数据,与之关联的值设置为默认值（前提FK字段需要设置默认值,一对一同理）
    on_delete=models.SET,         # 删除关联数据,
    a. 与之关联的值设置为指定值,设置：models.SET(值)
    b. 与之关联的值设置为可执行对象的返回值,设置：models.SET(可执行对象)
    '''
    owner = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name='作者')
    created_time = models.DateTimeField(auto_now=True,verbose_name='创建时间')

    #Model 元数据就是 "不是一个字段的任何数据" -- 比如排序选项, admin 选项等等.
    class Meta:
        '''
        verbose_name指定在admin管理界面中显示中文；verbose_name表示单数形式的显示，
        verbose_name_plural表示复数形式的显示；中文的单数和复数一般不作区别。
        '''
        verbose_name = verbose_name_plural = '分类'

    def __str__(self):
        return self.name

class Tag(models.Model):
    STATUS_NORMAL = 1
    STATUS_DELETE = 0
    STATUS_ITEMS = (
        (STATUS_NORMAL,'正常'),
        (STATUS_DELETE,'删除'),
    )

    name = models.CharField(max_length=10,verbose_name='名称')
    status = models.PositiveSmallIntegerField(default=STATUS_NORMAL,choices=STATUS_ITEMS,
        verbose_name='状态')
    owner = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name='作者')
    created_time = models.DateTimeField(auto_now_add=True,verbose_name='创建时间')

    class Meta:
        verbose_name = verbose_name_plural = '标签'

    def __str__(self):
        return self.name

class Post(models.Model):
    STATUS_NORMAL = 1
    STATUS_DELETE = 0
    STATUS_DRAFT = 2
    STATUS_ITEMS = (
        (STATUS_NORMAL,'正常'),
        (STATUS_DELETE,'删除'),
        (STATUS_DRAFT,'草稿'),
    )

    title = models.CharField(max_length=255,verbose_name='标题')
    '''
    null 是针对数据库而言，如果 null=True, 表示数据库的该字段可以为空，即在Null字段显示为YES。
    blank 是针对表单的，如果 blank=True，表示你的表单填写该字段的时候可以不填，但是对数据库来说，没有任何影响
    '''
    desc = models.CharField(max_length=1024,blank=True,verbose_name='摘要')
    content = models.TextField(verbose_name='正文',help_text='正文必须是markdown格式')
    status = models.PositiveSmallIntegerField(default=STATUS_NORMAL,
        choices=STATUS_ITEMS,verbose_name='状态')
    category = models.ForeignKey(Category,on_delete=models.DO_NOTHING,verbose_name='分类')
    tag = models.ManyToManyField(Tag,verbose_name='标签')
    owner = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name='作者')
    '''
    auto_now无论是你添加还是修改对象，时间为你添加或者修改的时间。
    auto_now_add为添加时的时间，更新对象时不会有变动。
    '''
    created_time = models.DateTimeField(auto_now_add=True,verbose_name='创建时间')

    class Meta:
        verbose_name = verbose_name_plural = '文章'
        ordering = ['-id'] #根据ID进行降序排列

    def __str__(self):
        return self.title
    



