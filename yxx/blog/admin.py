from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from django.contrib.admin.models import LogEntry
from django.http import HttpResponse
# from daterange_filter.filter import DateRangeFilter

from openpyxl import Workbook

from yxx.custom_site import custom_site
from .models import Post,Category,Tag
from .adminforms import PostAdminForm
from yxx.base_admin import BaseOwnerAdmin
# Register your models here.

class ExportExcelMixin(object):
    def export_as_excel(self, request, queryset):
        '''导出选中项为excel'''
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='application/msexcel')
        response['Content-Disposition'] = f'attachment; filename={meta}.xlsx'
        wb = Workbook()
        ws = wb.active
        ws.append(field_names)
        for obj in queryset:
            for field in field_names:
                data = [f'{getattr(obj, field)}' for field in field_names]
            row = ws.append(data)

        wb.save(response)
        return response
    export_as_excel.short_description = '导出Excel'

    def export_as_csv(self,request,queryset):
        '''导出选中项为csv'''
        meta = self.model._meta  # 用于定义文件名, 格式为: app名.模型类名
        field_names = [field.name for field in meta.fields]  # 模型所有字段名

        response = HttpResponse(content_type='application/msexcel')  # 定义响应内容类型
        response['Content-Disposition'] = f'attachment; filename={meta}.csv'  # 定义响应数据格式
        wb = Workbook()  # 新建Workbook
        ws = wb.active  # 使用当前活动的Sheet表
        ws.append(field_names)  # 将模型字段名作为标题写入第一行
        for obj in queryset:  # 遍历选择的对象列表
            for field in field_names:
                data = [f'{getattr(obj, field)}' for field in field_names]  # 将模型属性值的文本格式组成列表
            row = ws.append(data)  # 写入模型属性值
        wb.save(response)  # 将数据存入响应内容
        return response
    export_as_csv.short_description = '导出csv'    

class PostInline(admin.TabularInline):
    extra = 1
    model = Post

@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    list_display = ['object_repr','object_id','action_flag','user','change_message']
    list_per_page = 15

@admin.register(Category)
class CategoryAdmin(BaseOwnerAdmin,ExportExcelMixin):
    list_display = ('name','status','is_nav','created_time','owner','page_count')
    fields = ('name','status','is_nav')
    inlines = [PostInline,]
    list_per_page = 15
    actions = ['export_as_excel','export_as_csv',]

    def page_count(self,obj):
        return obj.post_set.count()
    page_count.short_description = '文章数量'

@admin.register(Tag)
class TagAdmin(BaseOwnerAdmin,ExportExcelMixin):
    list_display = ('name','status','created_time')
    fields = ('name','status')
    list_per_page = 15
    actions = ['export_as_excel','export_as_csv',]

class CategoryOwnerFilter(admin.SimpleListFilter):
    '''自定义过滤器只显示当前用户分类'''

    #标题展示
    title = '分类过滤器'
    #查询时URL参数名字
    parameter_name = 'owner_category'

    def lookups(self,request,model_admin):
        '''
        返回要展示的内容和查询用的ID
        '''
        return Category.objects.filter(owner=request.user).values_list('id','name')

    def queryset(self,request,queryset):
        '''
        根据URL查询的内容返回列表页数据
        '''
        category_id = self.value()
        if category_id:
            return queryset.filter(category_id=self.value())
        return queryset

@admin.register(Post)
class PostAdmin(BaseOwnerAdmin):
    form = PostAdminForm
    list_display = ['title','category','status','created_time','owner','operator']
    list_display_links = []
    # list_filter = ('category','created_time',
        
    # )
    search_fields = ['title','category__name']
    #用户在侧边栏的过滤器只看到自己创建的分类
    list_filter = (CategoryOwnerFilter,
        'created_time',
        #('created_time',DateRangeFilter),
    )
    list_per_page = 15
    #不显示的字段
    exclude = ('owner',)
    #控制页面布局
    fieldsets = (
        ('基础配置',{
            'description':'基础配置描述',
            'fields':(
                ('title','category'),
                'status',
            ),
        }),
        ('内容',{
            'fields':(
                'desc',
                'content',
            ),
        }),
        ('额外信息',{
            'classes':('wide',),
            'fields':('tag',),
        })
    )
    #设置多对多字段的显示 filter_horizontal filter_vertical  
    filter_horizontal = ('tag',)

    actions_on_top = True
    actions_on_bottom = True

    #编辑页面
    save_on_top = True

    def operator(self,obj):
        return format_html(
            '<a href="{}">编辑</a>',
            reverse('admin:blog_post_change',args=(obj.id,))
        )
    operator.short_description = '操作'

    class Media:
        css = {
            'all':('https://github.com/bootcdn/BootCDN/blob/1.0.1/ajax/libs/bootstrap/4.0.0-beta.2/css/bootstrap.min.css')
        }
        js = ('https://github.com/bootcdn/BootCDN/blob/1.0.1/ajax/libs/bootstrap/4.0.0-beta.2/js/bootstrap.bundle.js')        


admin.site.site_title = 'Yxx博客后台管理'
#登录页面的登录框的文字
admin.site.site_header = 'Yxx博客后台管理'
