from django.contrib.auth.models import User
from django.db import models
from django.utils.html import format_html
from django.utils.safestring import mark_safe


from yxx.settings.base import *

import os
import time
import uuid

def image_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    newFilename = time.strftime('%Y-%m-%d %H:%M:%S_',time.localtime(time.time())) + str(uuid.uuid4())[0:8] + '.' + ext
    print(newFilename)
    return os.path.join('static', newFilename)

# Create your models here.
class Link(models.Model):
    STATUS_NORMAL = 1
    STATUS_DELETE = 0
    STATUS_ITEM = (
        (STATUS_NORMAL,'正常'),
        (STATUS_DELETE,'删除'),
    )

    title = models.CharField(max_length=50,verbose_name='标题')
    href = models.URLField(verbose_name='链接') #默认长度200
    status = models.PositiveSmallIntegerField(default=STATUS_NORMAL,choices=STATUS_ITEM,
        verbose_name='状态')
    logo = models.ImageField(verbose_name='友链官方logo',upload_to=image_upload_to,default='Yxx')
    weight = models.PositiveSmallIntegerField(default=1,choices=zip(range(1,6),
                                                range(1,6)),verbose_name='权重',
                                                help_text='权重高展示顺序靠前')
    owner = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name='作者')
    created_time = models.DateTimeField(auto_now_add=True,verbose_name='创建时间')

    def image_data(self):
        return mark_safe(u'<img src="/{}" style="width:80px;height:80px" '.format(self.logo) )
        # return format_html(
        #     '<img src="/{}" style="width:80px;height:80px"> '.format(self.logo),
        # )
    image_data.short_description = 'logo'

    def delete(self):
        '''
        详情页面点击删除
        '''
        #上级目录
        lastPath = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
        path = os.path.join(lastPath,'yxx',self.logo.url).replace('\\','/')
        os.remove(path)
        print('link-model-delete')
        super(Link,self).delete()

    class Meta:
        verbose_name = verbose_name_plural = '友链'
        

class SideBar(models.Model):
    STATUS_SHOW = 1
    STATUS_HIDE = 0
    STATUS_ITEM = (
        (STATUS_SHOW,'展示'),
        (STATUS_HIDE,'隐藏'),
    )
    SIDE_TYPE = (
        (1,'HTML'),
        (2,'最新文章'),
        (3,'最热文章'),
        (4,'最近评论'),
    )
    title = models.CharField(max_length=50,verbose_name='标题')
    display_type = models.PositiveSmallIntegerField(default=1,choices=SIDE_TYPE,
                                                    verbose_name='展示类型')
    content = models.CharField(max_length=500,blank=True,verbose_name='内容',
                                help_text='如果设置的不是HTML类型，可为空')                                                
    status = models.PositiveSmallIntegerField(default=STATUS_SHOW,choices=STATUS_ITEM,
                                                verbose_name='状态')
    owner = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name='作者')
    created_time = models.DateTimeField(auto_now_add=True,verbose_name='创建时间')

    class Meta:
        verbose_name = verbose_name_plural = '侧边栏'                                            






