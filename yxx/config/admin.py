from django.contrib import admin

import os
import json

from .models import Link,SideBar
from yxx.base_admin import BaseOwnerAdmin



# Register your models here.
@admin.register(Link)
class LinkAdmin(BaseOwnerAdmin):
    list_display = ('title','href','status','weight','created_time','image_data','logo')
    fields = ('title','href','status','weight','logo')
    readonly_fields = ('image_data',)
    actions = ['delete_selecte']
    list_per_page = 15

    def save_model(self,request,obj,form,change):
        obj.owner = request.user
        return super(LinkAdmin,self).save_model(request,obj,form,change)    

    def delete_selecte(self,request,queryset):
        '''
        删除选中项
        取消掉默认的删除之后，调用删除，会调用model重写删除
        '''
        for item in queryset:
            item.delete()
    delete_selecte.short_description = '删除'    

    def get_actions(self, request):
        '''
        所有用户都不可见
        '''
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

@admin.register(SideBar)
class SideBarAdmin(BaseOwnerAdmin):
    list_display = ('title','display_type','content','created_time')
    fields = ('title','display_type','content')
    list_per_page = 15
          
    def save_model(self,request,obj,form,change):
        obj.owner = request.user
        return super(SideBarAdmin,self).save_model(request,obj,form,change)