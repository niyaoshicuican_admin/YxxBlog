from django.db import models

# Create your models here.
class SlowQuery(models.Model):
    start_time = models.DateTimeField(verbose_name='开始时间')
    user_host = models.CharField(max_length=512,verbose_name='用户/主机名')
    query_time = models.TimeField(verbose_name='查询时间')
    lock_time = models.TimeField(verbose_name='被锁时间')
    rows_sent = models.IntegerField(verbose_name='发送行')
    rows_examined = models.IntegerField(verbose_name='检查行')
    db = models.CharField(max_length=512,verbose_name='数据库名')
    last_insert_id = models.IntegerField(verbose_name='上次插入ID')
    insert_id = models.IntegerField(verbose_name='插入ID')
    server_id = models.IntegerField(verbose_name='服务ID')
    sql_text = models.CharField(max_length=512,verbose_name='SQL')
    thread_id = models.BigIntegerField(verbose_name='线程ID')

    class Meta:
        db_table = 'slowquery'
        managed = False #默认是ture，设成false django将不会执行建表和删表操作
        verbose_name = verbose_name_plural = '慢查询'        


class Sql(models.Model):
    event_time = models.DateTimeField(verbose_name='发生时间')
    user_host = models.CharField(max_length=512,verbose_name='用户/主机名')
    thread_id = models.BigIntegerField(verbose_name='线程ID')
    server_id = models.IntegerField(verbose_name='服务ID')
    command_type = models.CharField(max_length=64,verbose_name='命令类型')
    argument = models.CharField(max_length=9999,verbose_name='SQL')

    class Meta:
        db_table = 'vSql'
        managed = False #默认是ture，设成false django将不会执行建表和删表操作
        verbose_name = verbose_name_plural = 'Sql'       