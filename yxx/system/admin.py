from django.contrib import admin

from .models import SlowQuery,Sql

# Register your models here.
@admin.register(SlowQuery)
class LogEntryAdmin(admin.ModelAdmin):
    list_display = ('start_time','user_host','query_time','lock_time','rows_sent','rows_examined',
        'db','last_insert_id','insert_id','server_id','sql_text','thread_id')
    list_per_page = 15
    #取消列表页面的删除与编辑，设置为None，将不能进入详情页
    actions = None
    # list_editable = ()

@admin.register(Sql)
class LogEntryAdmin(admin.ModelAdmin):
    list_display = ('event_time','user_host','server_id','thread_id','command_type','argument')
    list_per_page = 15
    #取消列表页面的删除与编辑，设置为None，将不能进入详情页
    actions = None    