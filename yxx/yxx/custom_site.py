from django.contrib.admin import AdminSite

class CustomSite(AdminSite):
    site_header = 'Yxx'
    site_title = 'Yxx博客后台管理系统'
    index_title = '首页'

custom_site = CustomSite(name='cus_admin')